<?php

namespace OpenapiNextGeneration\JsonSchemaGeneratorPhp\OpenApi;

use OpenapiNextGeneration\JsonSchemaGeneratorPhp\SchemaContainer;
use OpenapiNextGeneration\OpenapiRoutesMapperPhp\Route;

class JsonSchemaGenerator
{
    /**
     * The created json schema draft
     *
     * @var string
     */
    const JSON_SCHEMA_DRAFT = 'http://json-schema.org/draft-07/schema#';
    /**
     * Only request or response bodys with a content type in this list will be used
     *
     * @var array
     */
    const SUPPORTED_CONTENT_TYPES = [
        'application/json'
    ];
    const BODY_TYPE_REQUEST = 0;
    const BODY_TYPE_RESPONSE = 1;

    /**
     * Create schema containers for request and response bodies of the given routes
     */
    public function processRoutes(Route $route): array
    {
        $schemaContainers = [];

        $methodSpecification = $route->getMethodSpecification();

        $schemaContainers = $this->processBody(
            $schemaContainers,
            $methodSpecification['requestBody']['content'] ?? [],
            $route->getPathName() . '/' . strtolower($route->getHttpMethod()) . '/request',
            self::BODY_TYPE_REQUEST
        );

        foreach ($methodSpecification['responses'] ?? [] as $responseCode => $contentSpecification) {
            $schemaContainers = $this->processBody(
                $schemaContainers,
                $contentSpecification['content'] ?? [],
                $route->getPathName() . '/' . strtolower($route->getHttpMethod()) . '/response/' . $responseCode,
                self::BODY_TYPE_RESPONSE
            );
        }

        return $schemaContainers;
    }

    /**
     * Process a body (request or response)
     * Creates all SchemaContainer objects needed and adds the to $schemaContainers
     *  returns the updated SchemaContainers
     */
    protected function processBody(array $schemaContainers, array $contentSpecification, string $path, int $bodyType)
    {
        foreach ($contentSpecification as $contentTypeName => $contentType) {
            if (!in_array($contentTypeName, self::SUPPORTED_CONTENT_TYPES, true)) {
                continue;   //skip non-supported content types
            }

            $bodySpecification = $contentType['schema'] ?? null;
            if ($bodySpecification !== null) {
                $schemaContainer = $this->buildSchemaContainer($bodySpecification, $bodyType);
                $schemaContainer->setPath($path);
                $schemaContainers[] = $schemaContainer;
            }
        }
        return $schemaContainers;
    }

    /**
     * Creates a SchemaContainer object
     *  adds metadata
     *  resolves the $specification into the container
     */
    protected function buildSchemaContainer(array $specification, int $bodyType): SchemaContainer
    {
        $schemaContainer = new SchemaContainer();

        $schemaContainer->setSchema(
            array_merge(
                [
                    '$schema' => self::JSON_SCHEMA_DRAFT
                ],
                $this->resolveSchema($specification, $bodyType)
            )
        );

        return $schemaContainer;
    }

    /**
     * Resolves a schema or sub-schema
     */
    protected function resolveSchema(array $specification, int $bodyType): array
    {
        $schema = $this->resolveType($specification);
        $type = $schema['type'];
        if (is_array($type)) {
            $type = reset($type);
        }

        switch ($type) {
            case 'object':
                $schema = $this->resolveObject($schema, $specification, $bodyType);
                break;
            case 'array':
                $schema = $this->resolveArray($schema, $specification, $bodyType);
                break;
            case 'string':
                $schema = $this->resolveString($schema, $specification);
                break;
            case 'integer':
                $schema = $this->resolveInteger($schema, $specification);
                break;
            case 'number':
                $schema = $this->resolveNumber($schema, $specification);
                break;
            case 'boolean':
                $schema = $this->resolveBoolean($schema, $specification);
                break;
            default:
                $schema = [];
                break;
        }

        $schema = $this->addDescription($schema, $specification);
        $schema = $this->addExamples($schema, $specification);
        $schema = $this->addDefault($schema, $specification);
        return $schema;
    }

    protected function resolveObject($schema, array $specification, int $bodyType): array
    {
        $schema['description'] = $specification['description'] ?? '';
        $schema['required'] = $specification['required'] ?? [];
        if (isset($schema['required']['$ref'])) {
            unset($schema['required']['$ref']);
        }

        $schema['properties'] = new \ArrayObject();
        $properties = $specification['properties'] ?? [];
        foreach ($properties as $propertyName => $property) {
            if (
                $bodyType === self::BODY_TYPE_REQUEST && ($property['readOnly'] ?? false) === true
                || $bodyType === self::BODY_TYPE_RESPONSE && ($property['writeOnly'] ?? false) === true
            ) {
                foreach ($schema['required'] as $key => $item) {
                    if ($item === $propertyName) {
                        unset($schema['required'][$key]);
                    }
                }
                $schema['required'] = array_values($schema['required']);
                continue;
            }
            $schema['properties'][$propertyName] = $this->resolveSchema($property, $bodyType);
        }

        return $schema;
    }

    protected function resolveArray(array $schema, array $specification, int $bodyType): array
    {
        $schema['items'] = $this->resolveSchema($specification['items'] ?? [], $bodyType);
        if (isset($specification['minItems'])) {
            $schema['minItems'] = $specification['minItems'];
        }
        if (isset($specification['maxItems'])) {
            $schema['maxItems'] = $specification['maxItems'];
        }
        if (isset($specification['uniqueItems'])) {
            $schema['uniqueItems'] = $specification['uniqueItems'];
        }

        return $schema;
    }

    protected function resolveString(array $schema, array $specification): array
    {
        $schema = $this->addEnum($schema, $specification);
        $schema = $this->addLengthMinMax($schema, $specification);
        $schema = $this->addPattern($schema, $specification);
        $schema = $this->addFormat($schema, $specification);

        return $schema;
    }

    protected function resolveInteger(array $schema, array $specification): array
    {
        $schema = $this->addEnum($schema, $specification);
        $schema = $this->addMinMax($schema, $specification);

        return $schema;
    }

    protected function resolveNumber(array $schema, array $specification): array
    {
        $schema = $this->addEnum($schema, $specification);
        $schema = $this->addMinMax($schema, $specification);

        return $schema;
    }

    protected function resolveBoolean(array $schema, array $specification): array
    {
        $schema = $this->addEnum($schema, $specification);
        
        return $schema;
    }

    /**
     * Adds the description to the schema if given in the specification
     * returns the updated schema
     */
    protected function addDescription(array $schema, array $specification): array
    {
        if (isset($specification['description'])) {
            $schema['description'] = $specification['description'];
        }
        return $schema;
    }

    /**
     * Adds examples to the schema if examples are in the specification
     * returns the updated schema
     */
    protected function addExamples(array $schema, array $specification): array
    {
        if (isset($specification['example'])) {
            //OpenApi single example case
            $schema['examples'] = [$specification['example']];
        }
        return $schema;
    }

    /**
     * Adds default value to the schema if default is in the specification
     * returns the updated schema
     */
    protected function addDefault(array $schema, array $specification): array
    {
        if (isset($specification['default'])) {
            $schema['default'] = $specification['default'];
        }
        return $schema;
    }

    protected function resolveType(array $specification) : array
    {
        $type = $specification['type'] ?? null;

        if (isset($specification['nullable'])) {
            $type = [
                $type,
                'null'
            ];
        }

        return [
            'type' => $type
        ];
    }

    /**
     * Adds enum to the schema if it is set in the specification
     * returns the updated schema
     */
    protected function addEnum(array $schema, array $specification): array
    {
        if (isset($specification['enum'])) {
            $schema['enum'] = $specification['enum'];
            if ($specification['nullable'] ?? null === true && !in_array(null, $schema['enum'], true)) {
                $schema['enum'][] = null;   //add null as allowed enum option if property is nullable
            }
        }
        return $schema;
    }

    /**
     * Adds minimum and maximum to the schema if it is set in the specification
     * returns the updated schema
     */
    protected function addMinMax(array $schema, array $specification): array
    {
        if (isset($specification['minimum'])) {
            $schema['minimum'] = $specification['minimum'];
        }
        if (isset($specification['maximum'])) {
            $schema['maximum'] = $specification['maximum'];
        }
        return $schema;
    }

    /**
     * Adds minLength and maxLength to the schema if it is set in the specification
     * returns the updated schema
     */
    protected function addLengthMinMax(array $schema, array $specification): array
    {
        if (isset($specification['minLength'])) {
            $schema['minLength'] = $specification['minLength'];
        }
        if (isset($specification['maxLength'])) {
            $schema['maxLength'] = $specification['maxLength'];
        }
        return $schema;
    }

    /**
     * Adds pattern to the schema if it is set in the specification
     * returns the updated schema
     */
    protected function addPattern(array $schema, array $specification): array
    {
        if (isset($specification['pattern'])) {
            $schema['pattern'] = $specification['pattern'];
        }
        return $schema;
    }

    /**
     * Adds format to the schema if it is set in the specification
     * returns the updated schema
     */
    protected function addFormat(array $schema, array $specification): array
    {
        if (isset($specification['format'])) {
            $schema['format'] = $specification['format'];
        }
        return $schema;
    }
}
