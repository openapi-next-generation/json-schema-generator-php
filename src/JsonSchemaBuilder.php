<?php

namespace OpenapiNextGeneration\JsonSchemaGeneratorPhp;

use OpenapiNextGeneration\GenerationHelperPhp\TargetDirectory;
use OpenapiNextGeneration\JsonSchemaGeneratorPhp\OpenApi\JsonSchemaGenerator;
use OpenapiNextGeneration\OpenapiRoutesMapperPhp\Route;
use OpenapiNextGeneration\OpenapiRoutesMapperPhp\RoutesSpecification;

class JsonSchemaBuilder
{
    /**
     * @var JsonSchemaGenerator
     */
    protected $schemaGenerator;

    public function __construct(JsonSchemaGenerator $schemaGenerator)
    {
        $this->schemaGenerator = $schemaGenerator;
    }

    /**
     * Create all schema.json files in $targetDirectory (recursive directory structure)
     *
     * @throws \Exception
     */
    public function buildSchemas(array $specification, string $targetDirectory)
    {
        $targetDirectory = TargetDirectory::getCanonicalTargetDirectory($targetDirectory);

        $routesSpecification = new RoutesSpecification($specification);
        /* @var $route Route */
        foreach ($routesSpecification->getUniqueRoutes() as $route) {
            $schemaContainers = $this->schemaGenerator->processRoutes($route);
            /* @var $schemaContainer SchemaContainer */
            foreach ($schemaContainers as $schemaContainer) {
                $directory = $targetDirectory . $schemaContainer->getPath() . '/';
                if (!is_dir($directory)) {
                    mkdir($directory, 0777, true);
                }
                file_put_contents($directory . 'schema.json', $schemaContainer->getSchemaAsJson());
            }
        }
    }
}